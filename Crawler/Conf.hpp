#pragma once

using namespace System;
using namespace System::Collections::Generic;

public ref class Conf
{
private:
	Dictionary<String^, String^>^ parameters;
public:
	Conf();
	void Set(String^ parameter, String^ value);
	String^ Get(String^ parameter);
	bool Has(String^ parameter);
};
