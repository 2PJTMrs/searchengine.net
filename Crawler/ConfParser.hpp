#pragma once

#include "Conf.hpp"

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;

public ref class ConfParser
{
private:
	static ConfParser^ instance;
	Conf^ defaultConf;
	Conf^ fileConf;
	ConfParser();
public:
	static property ConfParser^ Instance { ConfParser^ get() {
		if (instance == nullptr)
			instance = gcnew ConfParser();
		return instance;
	}}
	void DefaultConf();
	void ParseFile(String^ filename);
	String^ Get(String^ parameter);
};

