#pragma once

#include "stdafx.h"
#include "ResourceAnalyzer.hpp"

ResourceAnalyzer::ResourceAnalyzer() {
	intranet = ConfParser::Instance->Get("intranet");
}

void ResourceAnalyzer::Analyze(Resource^ resource)
{
	if (resource->ContentType->Contains("text/html")) {
		analyzeHtml(resource);
	}
	else if (resource->ContentType->Contains("text/plain")) {
		analyzePlain(resource);
	}
	else {
		analyzeBinary(resource);
	}
}

void ResourceAnalyzer::analyzeHtml(Resource^ resource)
{
	HtmlDocument^ document = gcnew HtmlDocument();
	document->Load(resource->ResponseBody);

	analyzeHtmlKeywords(resource, document);
	analyzeHtmlLinks(resource, document);
}

void ResourceAnalyzer::analyzeHtmlKeywords(Resource^ resource, HtmlDocument^ document)
{
	// Analyse du title de la page
	HtmlNode^ titleNode = document->DocumentNode->SelectSingleNode("//title");
	if (titleNode != nullptr)
	{
		String^ title = titleNode->InnerText;
		if (!String::IsNullOrEmpty(title))
		{
			resource->Name = convertToUTF8(resource, title);
			addKeywordsFromString(resource, title);
		}
		else
		{
			resource->Name = "&lt;No name&gt;";
		}
	}

	// Analyse des meta keywords
	HtmlNode^ keywordsNode = document->DocumentNode->SelectSingleNode("//meta[@name='keywords']");
	if (keywordsNode != nullptr)
	{
		String^ keywords = keywordsNode->GetAttributeValue("content", "");
		if (!keywords->Equals(""))
			addKeywordsFromString(resource, keywords);
	}

	// Analyse de la meta description
	HtmlNode^ descNode = document->DocumentNode->SelectSingleNode("//meta[@name='description']");
	if (descNode != nullptr)
	{
		String^ description = descNode->GetAttributeValue("content", "");
		if (!description->Equals(""))
			addKeywordsFromString(resource, description);
	}

	// Analyse des tags significatifs
	List<String^>^ tags = gcnew List<String^>();
	tags->Add("h1");
	tags->Add("h2");
	tags->Add("h3");
	tags->Add("h4");
	tags->Add("h5");
	tags->Add("h6");
	tags->Add("b");
	tags->Add("strong");

	for each (String^ tag in tags)
	{
		String^ xPath = "//" + tag;
		HtmlNodeCollection^ nodeList = document->DocumentNode->SelectNodes(xPath);
		if (nodeList != nullptr)
		{
			for each (HtmlNode^ node in nodeList)
			{
				addKeywordsFromString(resource, node->InnerText);
			}
		}
	}
}

void ResourceAnalyzer::addKeywordsFromString(Resource^ resource, String^ keywords)
{
	array<__wchar_t>^ separators = gcnew array<__wchar_t>(8);
	separators[0] = ',';
	separators[1] = ';';
	separators[2] = '.';
	separators[3] = '!';
	separators[4] = '?';
	separators[5] = ':';
	separators[6] = '\'';
	separators[7] = ' ';
	array<String^, 1>^ keywordArray = convertToUTF8(resource, keywords)->Trim()->Split(separators);

	for each (String^ keyword in keywordArray)
	{
		keyword->Trim();

		if (keyword->Length == 0)
			continue;

		if (!resource->Keywords->Contains(keyword))
			resource->Keywords->Add(keyword);
	}
}

void ResourceAnalyzer::analyzeHtmlLinks(Resource^ resource, HtmlDocument^ document)
{
	HtmlNodeCollection^ linkList = document->DocumentNode->SelectNodes("//a");
	if (linkList != nullptr)
	{
		analyzeLinkUrl(resource, linkList);
	}

	HtmlNodeCollection^ imgList = document->DocumentNode->SelectNodes("//img");
	if (imgList != nullptr)
	{
		analyzeLinkUrl(resource, imgList);
	}
}

void ResourceAnalyzer::analyzeLinkUrl(Resource^ resource, HtmlNodeCollection^ nodeList)
{
	List<String^>^ allowedProtocols = gcnew List<String^>();
	allowedProtocols->Add("http");
	allowedProtocols->Add("https");
	allowedProtocols->Add("ftp");

	for each (HtmlNode^ node in nodeList)
	{
		// On ne suit pas les liens avec rel="nofollow"
		String^ rel = node->GetAttributeValue("rel", "");
		if (rel->ToLower()->Equals("nofollow"))
			continue;

		String^ urlString = String::Empty;
		Uri^ url;

		// Une url int�ressante ? ^_^
		if (node->Name->Equals("a"))
			urlString = node->GetAttributeValue("href", "");
		else if (node->Name->Equals("img"))
			urlString = node->GetAttributeValue("src", "");

		if (urlString->Length > 0)
		{
			// On s'assure d'obtenir une url en UTF8
			urlString = convertToUTF8(resource, urlString);

			// Si c'est du relatif le chemin sera r�solu, sinon l'h�te de l'urlString sera pris en compte
			url = gcnew Uri(resource->Url, urlString);

			// L'url fait-elle partie de l'intranet ?
			bool acceptUrl;
			if (intranet->Equals("%"))
				acceptUrl = true;
			else
				acceptUrl = url->Authority->Length >= intranet->Length && url->Authority->Substring(url->Authority->Length - intranet->Length)->Equals(intranet);
			if (!acceptUrl)
				continue;

			// Le protocole est-il accept� ?
			if (!allowedProtocols->Contains(url->Scheme))
				continue;

			// Suppression de l'ancre
			if (!String::IsNullOrEmpty(url->Fragment)) {
				url = gcnew Uri(url->AbsoluteUri->Substring(0, url->AbsoluteUri->LastIndexOf("#")));
			}

			// Ajout du lien
			if (!resource->Links->Contains(url))
				resource->Links->Add(url);
		}
	}
}

void ResourceAnalyzer::analyzeBinary(Resource^ resource)
{
	assignNameFromUrl(resource);
}

void ResourceAnalyzer::analyzePlain(Resource^ resource)
{
	StreamReader^ reader = gcnew StreamReader(resource->ResponseBody);
	resource->Content = reader->ReadToEnd();
	assignNameFromUrl(resource);
}

String^ ResourceAnalyzer::convertToUTF8(Resource^ resource, String^ str)
{
	// Si c'est d�j� de l'UTF8 on retourne la string d'origine
	if (resource->Encoding == Encoding::UTF8)
		return str;

	// On r�cup�re les char � partir de l'encodage d'origine
	array<unsigned char>^ srcBytes = resource->Encoding->GetBytes(str);

	// On les convertit vers l'UTF8
	array<unsigned char>^ dstBytes = Encoding::Convert(resource->Encoding, Encoding::UTF8, srcBytes);

	// On retourne une cha�ne de caract�res UTF8 � partir de ces char
	return Encoding::UTF8->GetString(dstBytes);
}

void ResourceAnalyzer::assignNameFromUrl(Resource^ resource)
{
	Regex^ regex = gcnew Regex("/(?<name>[^/]+?)$");
	Match^ match = regex->Match(resource->Url->AbsoluteUri);
	if (match != nullptr && match->Success)
	{
		resource->Name = match->Groups["name"]->Value;
	}
}

