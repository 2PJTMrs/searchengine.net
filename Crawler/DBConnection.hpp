#pragma once

#include "stdafx.h"
#include "ConfParser.hpp"

using namespace System;
using namespace System::Data;
using namespace System::Data::SqlClient;
using namespace System::Security;


public ref class DBConnection
{
private:
	static SqlConnection^ instance;
public:
	static property SqlConnection^ Connection {
		SqlConnection^ get() {
			if (instance == nullptr) {
				ConfParser^ parser = ConfParser::Instance;
				SecureString^ securePwd = gcnew SecureString();

				for each (wchar_t c in parser->Get("connectionpassword")->ToCharArray())
					securePwd->AppendChar(c);

				securePwd->MakeReadOnly();
				SqlCredential^ credentials = gcnew SqlCredential(parser->Get("connectionuser"), securePwd);
				instance = gcnew SqlConnection(ConfParser::Instance->Get("connectionstring"), credentials);
			}
			return instance;
		}
	}
};

