#pragma once
#include "stdafx.h"

using namespace System;
using namespace System::IO;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace System::Net;

public ref class Resource
{
private:

public:
	Resource();
	property Uri^ Url;
	property String^ Name;
	property String^ Content;
	property Encoding^ Encoding;
	property List<Uri^>^ Links;
	property List<String^>^ Keywords;
	property HttpWebResponse^ Response;
	property Stream^ ResponseBody;
	property String^ ContentType;
	property bool IsDirectory;
};
