#include "stdafx.h"
#include "ConfParser.hpp"


ConfParser::ConfParser()
{
	ParseFile("crawler.conf");
	DefaultConf();
}

void ConfParser::DefaultConf()
{
	defaultConf = gcnew Conf();
	defaultConf->Set("ftpdelay", "10000");
	defaultConf->Set("httpdelay", "150");
	defaultConf->Set("httpintranet", ".lan");
	defaultConf->Set("urlpoolsize = ", "100");
	defaultConf->Set("connectionstring", "");
	defaultConf->Set("connectionuser", "");
	defaultConf->Set("connectionpawword", "");
}

void ConfParser::ParseFile(String^ filename)
{


	fileConf = gcnew Conf();

	StreamReader^ reader = gcnew StreamReader(gcnew FileStream(filename, FileMode::Open));

	while (!reader->EndOfStream)
	{
		String^ line = reader->ReadLine();

		int position = line->IndexOf("=");

		if (position != -1)
			fileConf->Set(line->Substring(0, position), line->Substring(position + 1));
	}

	reader->Close();
}

String^ ConfParser::Get(String^ parameter)
{
	if (fileConf->Has(parameter))
		return fileConf->Get(parameter);
	else
		return defaultConf->Get(parameter);
}
