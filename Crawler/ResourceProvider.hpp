#pragma once
#include "stdafx.h"
#include "DBConnection.hpp"
#include "Resource.hpp"


using namespace System;
using namespace System::Data;
using namespace System::Data::SqlClient;
using namespace System::Net;
using namespace System::Xml;
using namespace HtmlAgilityPack;

public ref class ResourceProvider
{
private:

public:
	ResourceProvider();
	Resource^ Find(Uri^ url);
	bool Persist(Resource^ resource);
};

