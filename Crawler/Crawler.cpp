#include "stdafx.h"
#include "Crawler.hpp"

using namespace System::Data::SqlClient;

Crawler::Crawler()
{
	intranet = ConfParser::Instance->Get("httpintranet");
	urlProvider = gcnew UrlProvider();
	resourceProvider = gcnew ResourceProvider();
	resourceAnalyzer = gcnew ResourceAnalyzer();
}

void Crawler::Crawl()
{
	ConfParser^ parser = ConfParser::Instance;
	int delay;
	if (!int::TryParse(parser->Get("delay"), delay)) {
		delay = 100;
	}
	try {
		while (true) {
			doCrawl();
			Thread::Sleep(delay);
		}
	}
	catch (Exception^ e)
	{
		Console::WriteLine("Unable to crawl : {0}", e->Message);
	}
	Console::ReadLine();
}

void Crawler::doCrawl()
{
	Uri^ url = urlProvider->GetNext();

	if (url->Scheme == "http" || url->Scheme == "https")
		handleHttpAddress(url);
	else if (url->Scheme == "ftp")
		handleFtpAddress(url);
	else
	{
		Console::WriteLine("<Unsupported protocol: {0}> Removing {1}", url->Scheme, url);
		urlProvider->Remove(url);
	}
}

void Crawler::handleHttpAddress(Uri^ url)
{
	bool acceptUrl;

	if (intranet->Equals("%"))
	{
		acceptUrl = true;
	}
	else
	{
		acceptUrl = url->Authority->Length >= intranet->Length && url->Authority->Substring(url->Authority->Length - intranet->Length)->Equals(intranet);
	}

	if (acceptUrl)
	{
		Console::WriteLine("Parsing {0}", url);
		Resource^ resource;
		try {
			resource = resourceProvider->Find(url);
		}
		catch (WebException^ e) {
			Console::WriteLine("<{0}> Removing {1}", e->Status, url);
			urlProvider->Remove(url);
		}
		if (resource != nullptr)
		{
			resourceAnalyzer->Analyze(resource);
			urlProvider->InsertRange(resource->Links);
			resourceProvider->Persist(resource);
		}
	}
	else
	{
		Console::WriteLine("Removing extranet address {0}", url);
		urlProvider->Remove(url);
	}
}

void Crawler::handleFtpAddress(Uri^ url)
{
	Console::WriteLine("Parsing {0}", url);

	FtpWebRequest^ request = (FtpWebRequest^)WebRequest::Create(url);
	request->Method = WebRequestMethods::Ftp::ListDirectoryDetails;

	FtpWebResponse^ response = (FtpWebResponse^)request->GetResponse();

	List<Resource^>^ objectList = handleFtpDirectory(response);

	for each (Resource^ resource in objectList)
	{
		if (resource->IsDirectory) {
			handleFtpAddress(resource->Url);
		}
		else {
			Console::WriteLine("    Adding {0}", resource->Url);
			resourceProvider->Persist(resource);
		}
	}

	response->Close();
}

List<Resource^>^ Crawler::handleFtpDirectory(FtpWebResponse^ response)
{
	List<Resource^>^ list = gcnew List<Resource^>();

	Stream^ responseStream = response->GetResponseStream();
	StreamReader^ reader = gcnew StreamReader(responseStream);

	array<__wchar_t>^ separators = gcnew array<__wchar_t>(3);
	separators[0] = ' ';
	separators[1] = '-';
	separators[2] = '_';

	array<String^, 1>^ keywordArray = response->ResponseUri->ToString()->Trim()->Split(separators);

	while (!reader->EndOfStream)
	{
		String^ line = reader->ReadLine();
		Resource^ resource = gcnew Resource();

		resource->IsDirectory = line->Substring(0, 1) == "d";
		resource->Name = line->Remove(0, 49); // 49 caract�res avant d'arriver � ceux du nom du fichier

		if (resource->IsDirectory)
			resource->Name = resource->Name + "/";

		resource->Url = gcnew Uri(response->ResponseUri, resource->Name);

		for each (String^ keyword in keywordArray)
		{
			resource->Keywords->Add(keyword);
		}
		resource->Keywords->Add(resource->Name);

		list->Add(resource);
	}

	reader->Close();

	return list;
}
