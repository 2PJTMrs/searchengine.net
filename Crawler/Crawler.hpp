#pragma once

#include "ResourceAnalyzer.hpp"
#include "UrlProvider.hpp"
#include "ResourceProvider.hpp"

using namespace System::Net;
using namespace System::Threading;

ref class Crawler
{
private:
	UrlProvider^ urlProvider;
	ResourceProvider^ resourceProvider;
	ResourceAnalyzer^ resourceAnalyzer;
	String^ intranet;
	void doCrawl();
	void handleHttpAddress(Uri^ url);
	void handleFtpAddress(Uri^ url);
	List<Resource^>^ handleFtpDirectory(FtpWebResponse^ response);
public:
	Crawler();
	void Crawl();
};

