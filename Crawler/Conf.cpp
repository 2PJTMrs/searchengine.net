#include "stdafx.h"
#include "Conf.hpp"

Conf::Conf()
{
	parameters = gcnew Dictionary<String^, String^>();
}

void Conf::Set(String^ parameter, String^ value)
{
	if (parameters->ContainsKey(parameter))
		parameters[parameter] = value;
	else
		parameters->Add(parameter, value);
}

String^ Conf::Get(String^ parameter)
{
	if (parameters->ContainsKey(parameter))
		return parameters[parameter];
	else
		return String::Empty;
}

bool Conf::Has(String^ parameter)
{
	return parameters->ContainsKey(parameter);
}
