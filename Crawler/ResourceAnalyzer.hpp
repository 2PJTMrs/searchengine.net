#pragma once
#include "stdafx.h"
#include "Resource.hpp"
#include "ConfParser.hpp"

using namespace HtmlAgilityPack;
using namespace System;
using namespace System::IO;
using namespace System::Text;
using namespace System::Text::RegularExpressions;
using namespace System::Collections::Generic;

ref class ResourceAnalyzer
{
private:
	String^ intranet;
	void addKeywordsFromString(Resource^ resource, String^ keywords);
	void analyzeBinary(Resource^ resource);
	void analyzePlain(Resource^ resource);
	void analyzeHtml(Resource^ resource);
	void analyzeHtmlKeywords(Resource^ resource, HtmlDocument^ document);
	void analyzeHtmlLinks(Resource^ resource, HtmlDocument^ document);
	void analyzeLinkUrl(Resource^ resource, HtmlNodeCollection^ nodeList);
	String^ convertToUTF8(Resource^ resource, String^ str);
	void assignNameFromUrl(Resource^ resource);
public:
	ResourceAnalyzer();
	void Analyze(Resource^ resource);
};

