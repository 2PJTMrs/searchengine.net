#pragma once

using namespace System;
using namespace System::Runtime::Serialization;

public ref class ProviderException : public Exception
{
public:
	ProviderException() : Exception() { }
	ProviderException(String^ message) : Exception(message) { }
	ProviderException(String^ message, Exception^ innerException) : Exception(message, innerException) { }
protected:
	ProviderException(SerializationInfo^ info, StreamingContext context) : Exception(info, context) { }
};
