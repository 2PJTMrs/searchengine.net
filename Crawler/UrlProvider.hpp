#pragma once
#include "stdafx.h"
#include "DBConnection.hpp"
#include "ProviderException.hpp"

using namespace System;
using namespace System::Collections::Generic;

public ref class UrlProvider
{
private:
	int lastId;
	int listSize;
	Queue<Uri^>^ list;
	void getNextList(bool isRecursion);
	void getNextList();
public:
	UrlProvider();
	Uri^ GetNext();
	void InsertRange(List<Uri^>^ urls);
	void Remove(Uri^ url);
};
