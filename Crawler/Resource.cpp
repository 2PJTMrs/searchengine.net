#pragma once

#include "stdafx.h"
#include "Resource.hpp"

Resource::Resource()
{
	Name = String::Empty;
	ContentType = String::Empty;
	Content = String::Empty;
	Links = gcnew List<Uri^>();
	Keywords = gcnew List<String^>();
}
