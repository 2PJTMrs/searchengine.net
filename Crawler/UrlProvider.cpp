
#pragma once

#include "stdafx.h"
#include "UrlProvider.hpp"

UrlProvider::UrlProvider()
{
	lastId = 0;
	listSize = int::Parse(ConfParser::Instance->Get("urlpoolsize"));
	list = gcnew Queue<Uri^>();
}

Uri^ UrlProvider::GetNext()
{
	if (list->Count == 0)
	{
		getNextList();
	}

	return list->Dequeue();
}

void UrlProvider::getNextList(bool isRecursion)
{
	SqlConnection^ connection = DBConnection::Connection;


	SqlCommand^ query = gcnew SqlCommand(
		String::Concat(
		"SELECT Id, Url FROM dbo.Urls WHERE Id > ",
		lastId,
		" ORDER BY Id OFFSET 0 ROWS FETCH NEXT ",
		listSize,
		" ROWS ONLY"),
		connection);

	connection->Open();
	SqlDataReader^ reader = query->ExecuteReader();

	while (reader->Read())
	{
		IDataRecord^ record = (IDataRecord^)reader;

		lastId = (int)record[0];

		Uri^ url = gcnew Uri((String^)record[1]);
		list->Enqueue(url);
	}
	reader->Close();
	connection->Close();

	if (list->Count == 0)
	{
		if (!isRecursion)
		{
			lastId = 0;
			getNextList(true);
		}
		else
		{
			throw gcnew ProviderException("No URL found!");
		}
	}
}

void UrlProvider::getNextList()
{
	getNextList(false);
}

void UrlProvider::InsertRange(List<Uri^>^ urls)
{
	if (urls->Count > 0) {
		SqlConnection^ connection = DBConnection::Connection;

		String^ queryString = "INSERT INTO dbo.Urls (Url) VALUES ";

		SqlCommand^ query = gcnew SqlCommand("INSERT INTO dbo.Urls (Url) VALUES (@Url)", connection);

		query->Parameters->Add(gcnew SqlParameter("@Url", SqlDbType::VarChar, 3072));

		connection->Open();

		bool firstLoop = true;
		for each (Uri^ url in urls)
		{
			query->Parameters[0]->Value = url->AbsoluteUri;
			if (firstLoop) {
				firstLoop = false;
				query->Prepare();
			}
			try {
				query->ExecuteNonQuery();
				Console::WriteLine("   Adding {0}", url->AbsoluteUri);
			}
			catch (SqlException^ e) // Unique constraint violation, rien � traiter
			{}
		}

		connection->Close();
	}
}

void UrlProvider::Remove(Uri^ url)
{
	SqlConnection^ connection = DBConnection::Connection;

	SqlCommand^ query = gcnew SqlCommand("DELETE FROM dbo.Urls WHERE Url = @Url", connection);

	query->Parameters->Add(gcnew SqlParameter("@Url", SqlDbType::VarChar, 3072))->Value = url->AbsoluteUri;

	connection->Open();

	query->Prepare();
	query->ExecuteNonQuery();

	connection->Close();
}
