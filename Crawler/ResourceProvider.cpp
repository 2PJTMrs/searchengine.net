#pragma once
#include "stdafx.h"
#include "ResourceProvider.hpp"


ResourceProvider::ResourceProvider() { }

Resource^ ResourceProvider::Find(Uri^ url)
{
	HttpWebRequest^ request = WebRequest::CreateHttp(url);

	Resource^ resource = gcnew Resource();

	request->AllowAutoRedirect = true;
	request->UserAgent = "InternalCrawler";
	resource->Response = (HttpWebResponse^)request->GetResponse();

	try {
		resource->Encoding = Encoding::GetEncoding(resource->Response->CharacterSet);
	}
	catch (Exception^ e) {
		resource->Encoding = Encoding::UTF8;
	}

	resource->Url = url;
	resource->ResponseBody = resource->Response->GetResponseStream();
	resource->ContentType = resource->Response->ContentType;

	return resource;
}

bool ResourceProvider::Persist(Resource^ resource)
{
	SqlConnection^ connection = DBConnection::Connection;
	connection->Open();

	SqlCommand^ findQuery = gcnew SqlCommand("SELECT Id FROM dbo.Resources WHERE Url = @Url", connection);

	SqlParameter^ urlParam = gcnew SqlParameter("@Url", SqlDbType::VarChar, 3072);
	urlParam->Value = resource->Url->AbsoluteUri;

	findQuery->Parameters->Add(urlParam);
	findQuery->Prepare();

	Object^ id = findQuery->ExecuteScalar();

	SqlCommand^ resourceQuery;
	if (id != nullptr && (int)id > 0)
	{
		resourceQuery = gcnew SqlCommand("UPDATE dbo.Resources SET Content = @Content, Keywords = @Keywords, Name = @Name WHERE Id = @Id", connection);

		SqlParameter^ idParam = gcnew SqlParameter("@Id", SqlDbType::Int);
		idParam->Value = (int)id;

		resourceQuery->Parameters->Add(idParam);
	}
	else
	{
		resourceQuery = gcnew SqlCommand("INSERT INTO dbo.Resources (Url, Content, Keywords, Name) VALUES(@Url, @Content, @Keywords, @Name)", connection);
		SqlParameter^ urlParamCpy = gcnew SqlParameter("@Url", SqlDbType::VarChar, 3072);
		urlParamCpy->Value = resource->Url->AbsoluteUri;
		resourceQuery->Parameters->Add(urlParamCpy);
	}

	SqlParameter^ contentParam = gcnew SqlParameter("@Content", SqlDbType::Text, resource->Content != nullptr ? resource->Content->Length : 1);
	contentParam->Value = resource->Content != nullptr ? resource->Content : String::Empty;

	String^ keywordsString = "";
	for each (String^ str in resource->Keywords)
		keywordsString = String::Concat(keywordsString, " ", str);
	SqlParameter^ keywordsParam = gcnew SqlParameter("@Keywords", SqlDbType::Text, keywordsString->Length);
	keywordsParam->Value = keywordsString;

	SqlParameter^ nameParam = gcnew SqlParameter("@Name", SqlDbType::VarChar, 300);
	nameParam->Value = resource->Name;

	resourceQuery->Parameters->Add(contentParam);
	resourceQuery->Parameters->Add(keywordsParam);
	resourceQuery->Parameters->Add(nameParam);

	int result = resourceQuery->ExecuteNonQuery();
	connection->Close();

	return result == 0;
}

